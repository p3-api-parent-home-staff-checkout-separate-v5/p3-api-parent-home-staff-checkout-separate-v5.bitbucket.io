/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9394243197560733, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.375, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9997982732187525, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9852716143840857, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.9179263108171941, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.1422172452407615, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.9498400150574063, 500, 1500, "me"], "isController": false}, {"data": [0.9488771225123243, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.8715698787492023, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.9815083393763597, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.006211180124223602, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 60346, 0, 0.0, 247.76722566532789, 11, 3481, 141.0, 440.0, 1020.0, 2088.970000000005, 200.3073685468372, 954.8001712870146, 267.8263669560556], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1128, 0, 0.0, 1328.0975177304972, 603, 2494, 1309.5, 1749.6000000000001, 1885.6499999999999, 2072.0, 3.7529569507890193, 2.3712530733989214, 4.64355122719696], "isController": false}, {"data": ["getLatestMobileVersion", 24786, 0, 0.0, 59.97522795126284, 11, 1061, 32.0, 140.0, 170.0, 232.0, 82.61201417196337, 55.26291963651847, 60.91022529280503], "isController": false}, {"data": ["findAllConfigByCategory", 7842, 0, 0.0, 190.48673807702104, 28, 1114, 165.0, 312.0, 416.0, 640.1399999999994, 26.13538274904767, 29.555442600973826, 33.945370172102926], "isController": false}, {"data": ["getNotifications", 4234, 0, 0.0, 353.0429853566376, 60, 1577, 273.0, 773.0, 987.0, 1203.6499999999996, 14.108113331400714, 117.28746952169364, 15.692520590298257], "isController": false}, {"data": ["getHomefeed", 893, 0, 0.0, 1677.4266517357205, 824, 2860, 1676.0, 2110.6, 2244.699999999999, 2520.5199999999977, 2.9700039577880286, 36.18822205207152, 14.861621366900254], "isController": false}, {"data": ["me", 5313, 0, 0.0, 281.24938829286657, 61, 1294, 231.0, 502.0, 714.0, 937.7199999999993, 17.716259745109937, 23.340861036524572, 56.141858274298585], "isController": false}, {"data": ["findAllChildrenByParent", 5477, 0, 0.0, 272.9435822530574, 54, 1328, 214.0, 512.0, 744.1999999999989, 1041.4400000000005, 18.25155623092201, 20.49735318902374, 29.15971288455899], "isController": false}, {"data": ["getAllClassInfo", 3134, 0, 0.0, 477.37396298659837, 58, 1617, 364.0, 1048.0, 1203.25, 1377.5500000000006, 10.441167651703436, 29.813542992357355, 26.80647437141439], "isController": false}, {"data": ["findAllSchoolConfig", 6895, 0, 0.0, 216.74938361131237, 39, 1120, 190.0, 348.40000000000055, 450.0, 707.1999999999998, 22.977282648902456, 501.1022227688375, 16.85150319270092], "isController": false}, {"data": ["getChildCheckInCheckOut", 644, 0, 0.0, 2330.4829192546576, 1348, 3481, 2277.0, 2885.0, 3064.0, 3240.6499999999996, 2.1376670882252387, 142.5285355943146, 9.83660871066145], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 60346, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
